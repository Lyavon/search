#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>

#include "crutch.h"


typedef struct cstring_t {
    size_t len;
    void *struct_begin;
    size_t ref_cnt;
    cdestructor_t destructor;
    uint8_t data[];
} cstring_t;


size_t cstr_len(const char *src)
{
    cstring_t *str = container_of(src, cstring_t, data);
    return str->len;
}


char *cstr_init(size_t len)
{
    cstring_t *buf = malloc(sizeof(*buf) + len + 1);
    if (buf == NULL) {
        print_e("Can't allocate buffer for cstring_t of size %ju.\n", sizeof(*buf) + len + 1);
        return NULL;
    }
    buf->struct_begin = buf;
    buf->ref_cnt = 1;
    buf->destructor = NULL;
    return (char *)&buf->data;
}


char *cstr_strncpy(const char *src, size_t n)
{
    size_t len;
    char *str;

    len = strlen(src);
    len = min(len, n);
    str = cstr_init(len);
    if (str == NULL) {
        print_e("Can't allocate new cstring for %s.\n", src);
        return NULL;
    }

    strncpy(str, src, len);
    return str;
}


char *cstr_strcpy(const char *src)
{
    size_t len;
    char *str;

    len = strlen(src);
    str = cstr_init(len);
    if (str == NULL) {
        print_e("Can't allocate new cstring for %s.\n", src);
        return NULL;
    }

    strcpy(str, src);
    return str;
}


char *cstr_copy(const char *src)
{
    char *dst = cstr_init(cstr_len(src));
    if (dst == NULL) {
        print_e("Can't allocate new cstring for %s.\n", src);
        return NULL;
    }
    strcpy(dst, src);
    return dst;
}


char *cstr_join(const char *lhs, ...)
{
    va_list ap;
    const char *arg;
    char *dst;
    size_t total_len, current_len;

    total_len = current_len = cstr_len(lhs);

    for (va_start(ap, lhs); (arg = va_arg(ap, const char *)) != NULL;)
        total_len += cstr_len(arg);
    va_end(ap);

    dst = cstr_init(total_len);
    if (dst == NULL) {
        print_e("Can't allocate new cstring of size %ju.\n", total_len);
        return NULL;
    }
    strcpy(dst, lhs);

    for (va_start(ap, lhs); (arg = va_arg(ap, const char *)) != NULL;) {
        strcpy(dst + current_len, arg);
        current_len += cstr_len(arg);
    }
    va_end(ap);

    return dst;
}


char *cstr_raw_join(const char *lhs, ...)
{
    va_list ap;
    const char *arg;
    char *dst;
    size_t total_len, current_len;

    total_len = current_len = strlen(lhs);

    for (va_start(ap, lhs); (arg = va_arg(ap, const char *)) != NULL;)
        total_len += strlen(arg);
    va_end(ap);

    dst = cstr_init(total_len);
    if (dst == NULL) {
        print_e("Can't allocate new cstring of size %ju.\n", total_len);
        return NULL;
    }
    strcpy(dst, lhs);

    for (va_start(ap, lhs); (arg = va_arg(ap, const char *)) != NULL;) {
        strcpy(dst + current_len, arg);
        current_len += strlen(arg);
    }
    va_end(ap);

    return dst;
}


char *cstr_slice(const char *str, ssize_t low, ssize_t high, ssize_t step)
{
    ssize_t new_len, len = cstr_len(str);
    ssize_t abs_low = low >= 0 ? low : low + len;
    ssize_t abs_high = high >= 0 ? high : high + len;
    char *new;

    if (step > 0)
        for (new_len = 0; (abs_low + step * new_len) <= abs_high; new_len++);
    else
        for (new_len = 0; (abs_low + step * new_len) >= abs_high; new_len++);

        new = cstr_init(new_len);
        if (new == NULL) {
            print_e("Can't allocate new cstring of len = %ju.\n", new_len);
            return NULL;
        }
        new[new_len] = '\0';
        for (; new_len; new_len--)
            new[new_len - 1] = str[abs_low +  step * (new_len - 1)];
        return new;

    return NULL;
}


bool cstr_contains(const char *str, const char *pattern)
{
    return !!strstr(str, pattern);
}


bool cstr_startswith(const char *str, const char *pattern)
{
    char *found = strstr(str, pattern);
    if (found == str)
        return true;
    return false;
}


bool cstr_endswith(const char *str, const char *pattern)
{
    return !!strstr(str + cstr_len(str) - cstr_len(pattern), pattern);
}
