#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>

#include "crutch.h"


typedef struct cmem_t {
    void *struct_begin;
    size_t ref_cnt;
    cdestructor_t destructor;
    uint8_t data[];
} cmem_t;


static int cmalloc_destructor(void *data)
{
    int rc = 0;
    cmem_t *buf = container_of(data, cmem_t, data);

    if (buf->destructor != NULL)
        rc = buf->destructor(data);

    free(buf->struct_begin);
    return rc;
}


void *cmalloc(size_t size, cdestructor_t destructor)
{
    cmem_t *buf = malloc(sizeof(*buf) + size);
    if (buf == NULL) {
        print_e("Can't allocate buffer for cmem_t.\n");
        return NULL;
    }

    buf->struct_begin = buf;
    buf->destructor = destructor;
    buf->ref_cnt = 1;
    return (&buf->data);
}


void *cincref__(void *data)
{
    size_t *ref_cnt;

    if (data == NULL)
        return NULL;

    ref_cnt = data - offsetof(cmem_t, data) + offsetof(cmem_t, ref_cnt);
    ++*ref_cnt;
    return data;
}


int cdecref__(void **data)
{
    size_t *ref_cnt;
    int rc = 0;

    if (data == NULL || *data == NULL)
        return 0;

    ref_cnt = *data - offsetof(cmem_t, data) + offsetof(cmem_t, ref_cnt);
    if (--*ref_cnt == 0)
        rc = cmalloc_destructor(*data);

    *data = NULL;
    return rc;
}
