#ifndef CRUTCH_H
#define CRUTCH_H

#include <stdint.h>
#include "crutch_debug.h"
#include <stddef.h>
#include <stdbool.h>

#ifdef __cplusplus
    extern "C" {
#endif

#ifndef min
#define min(x, y) ((x < y) ? x : y)
#endif

#ifndef max
#define max(x, y) ((x > y) ? x : y)
#endif

/*
 * container_of - cast a member of a structure out to the containing structure
 * @ptr:        the pointer to the member.
 * @type:       the type of the container struct this is embedded in.
 * @member:     the name of the member within the struct.
 *
 */
#ifndef container_of
#define container_of(ptr, type, member) (type *)((uint8_t *)(ptr) - offsetof(type,member))
#endif

/*
 * Crutch allocator strives to ease memory management by refcounting and
 * destructor usage. Each function is NULL-safe, no thread safety though.
 *
 * It is expected from allocator's derivatives creators to define several
 * fields consequtively above the data[], given to the end user, for proper
 * refcounting (in order to use cincref and cdecref):
 *
 * typedef struct cmem_t {
 *     void *struct_begin;
 *     size_t ref_cnt;
 *     cdestructor_t destructor;
 *     uint8_t data[];
 * } cmem_t;
 *
 * The cmem_t's layout is presented above. In order to comply with cincref
 * and decref, all the derivative structures have to have their versions
 * of struct_begin, ref_cnt, destructor in the corresponding offsets from data.
 * Destructor is optional in case no finalizing needs to be done before memory
 * deallocation.
 */


/*
 * Destructor callback for any memory allocated with cmalloc-compatible API.
 */
typedef int (*cdestructor_t)(void *);


/*
 * Decrease refcount callback for any memory allocated with cmalloc-compatible
 * API.
 */
typedef int (*cdecref_t)(void **);


/*
 * Increment refcount callback for any memory allocated with cmalloc-compatible
 * API.
 */
typedef void *(*cincref_t)(void *);


/*
 * Allocate required amount of data with refcounting and destructor.
 */
void *cmalloc(size_t size, cdestructor_t destructor);


/*
 * Increment refcount for any cmalloc-compatible allocated data. Return ptr to
 * data.
*/
void *cincref__(void *data);
#define cincref(data_ptr) (cincref__((void *) data_ptr))

/*
 * Decrement refcount for any cmalloc-compatible allocated data. Nullify the
 * provided ptr.Deallocate the object if refcount drops to zero.
*/
int cdecref__(void **data);
#define cdecref(data_ptr) (cdecref__((void **) data_ptr))


/*
 * Crutch list is an attempt to make lists on top of cmalloc reusable for any
 * singular cmalloc object while being as lightweight as possible. It is a
 * double linked list.
 *
 * In order to reduce amount of hadles, double references are used in the
 * iteration protocol. Single dereference provides a reference to the payload.
 *
 * Crutch list doesn't incref data on assignment. This is made for the sake of
 * convenience of generating an object straight inside of a functions such as
 * clist_append, ..._prepend, ..._insert, etc. For instance:
 *     clist_t *l = clist_init(deref_t);
 *     clist_append(l, cstr_strcpy("example\n");
 * So in order to put a shared data:
 *     clist_prepend(l, cstr_ref(shared_cstr));
 */


/*
 * A handle for clists.
 */
typedef struct clist_t clist_t;


/*
 * Initialize new clist.
 */
clist_t *clist_init(cdecref_t data_deref);


/*
 * Get an iterator from the very beginning of the clist. NULL value on empty
 * clist.
 */
void **clist_iter(clist_t *list);


/*
 * Get an iterator from the very end of the clist. NULL value on empty clist.
 */
void **clist_iter_rev(clist_t *list);


/*
 * Next value from the following list item, NULL on empty clist.
 */
void **clist_next(void **le_data);


/*
 * Prev value from the following list item, NULL on empty clist.
 */
void **clist_prev(void **le_data);


/*
 * Increment ref_count. Returns a provided pointer.
 */
clist_t *clist_ref(clist_t *list);


/*
 * Decrement list ref_count. Decrements all the contained values after
 * dropping ref_cnt to 0. Zero on success.
 */
int clist_deref(clist_t **list);


/*
 * Return the length of the given field.
 */
int clist_length(clist_t *list);


/*
 * Insert a provided item between current items at the end.
 *
 * N.B.! data refcount is not incremented on adding.
 */
int clist_append(clist_t *list, void *data);


/*
 * Insert a provided item at the beginning.
 *
 * N.B.! data refcount is not incremented on adding.
 */
int clist_prepend(clist_t *list, void *data);


/*
 * Insert a provided item between current items at a given position.
 *
 * N.B.! data refcount is not incremented on adding.
 */
int clist_insert(clist_t *list, ssize_t at, void *data);


#define clist_foreach(list, le) \
    for ((le) = (typeof (le))clist_iter((list)); (le) != NULL; (le) = (typeof (le))clist_next((void **)(le)))

#define clist_foreach_safe(list, le, next_le) \
    for ((le) = clist_iter((list)), (next_le) = clist_next(le); (le); (le) = (next_le), (next_le) = clist_next(next_le))


/*
 * Crutch strings is an attempt to make string usage convenience closer to
 * higher level languages without loosing backwards compatibility with the
 * standart library and affecting perfomance too much. cstrings are made
 * compatible with cmalloc (in particular, cincref and cdecref are used for
 * refcounting).
*/


/*
 * Make copy of the provided cstring.
 */
char *cstr_copy(const char *src);


/*
 * New cstring from the given raw string.
 */
char *cstr_strcpy(const char *src);


/*
 * New cstring from max n bytes from the given raw string.
 */
char *cstr_strncpy(const char *src, size_t n);


/*
 * Allocate new cstring with the given size (0-byte adds internally).
 */
char *cstr_init(size_t len);


/*
 * Concatenate arbitrary number of cstrings into one. Use NULL at the end to
 * specify the end of the arguments list.
 */
char *cstr_join(const char *lhs, ...);


/*
 * Join arbitrary number of raw strings into new cstring. Use NULL at the end
 * to specify the end of the arguments list.
 */
char *cstr_raw_join(const char *lhs, ...);


/*
 * Return a string with corresponding slice.
 *
 * !!! Due to the inability to skip arguments, as in python, slice INCLUDES
 * upper boundary !!!.
 *
 * Negative indexes work as in python. Do not exceed length, though, no
 * modulo division is used.
 */
char *cstr_slice(const char *str, ssize_t low, ssize_t high, ssize_t step);


/*
 * Check whether cstring pattern occurs in cstring str.
 */
bool cstr_contains(const char *str, const char *pattern);


/*
 * Check whether cstring str starts with ctring pattern.
 */
bool cstr_startswith(const char *str, const char *pattern);


/*
 * Check whether cstring str ends with cstring pattern.
 */
bool cstr_endswith(const char *str, const char *pattern);



/*
 * cqueue is a lightweight interthread blocking message queue based on
 * pthreads.
 */

/*
 * Handle for cqueue.
 */
struct cqueue_t;
typedef struct cqueue_t cqueue_t;


/*
 * Create new cqueue and return handle thereof. max_size will take effect
 * being greater than zero. Return NULL in case of error.
 */
cqueue_t *cqueue_create(int max_size);


/*
 * Invalidate provided cqueue_t. Zero is returned on success, -1 otherwise.
 */
int cqueue_destroy(cqueue_t *cq);


/*
 * Put new item to the provided cqueue. data is not copied and expected to be
 * deleted by a consumer. NULL is permitted to be passed, though not
 * recommended. Returns 0 on success, -1 otherwise.
 */
int cqueue_put(cqueue_t *cq, void *data);


/*
 * Get element from the provided queue. NULL can be returned either on error
 * or in case it was provided by a producer.
 */
void *cqueue_get(cqueue_t *cq);


/*
 * Obtain size from the provided queue. Returns -1 on error.
 */
int cqueue_size(cqueue_t *cq);



/*
 * cchannel_t is a structure providing a way to return a single result from a
 * dispatched to a ctpool job.
 *
 * It is designed in a way that a producer can store only a single value and a
 * consumer can obtain only a single value.
 */
typedef struct cchannel_t cchannel_t;


/*
 * Create channel between a producer and a consumer. NULL for failure.
 */
cchannel_t *cchannel_create(void);


/*
 * Syncronously obtain a result from a channel. NULL-safe. The channel will be
 * destroyed after obtaining the value. Returns 0 on success, -1 otherwise.
 */
int cchannel_result(cchannel_t **ch, void **result);


/*
 * Put a value inside a channel. NULL-safe. The Channel will become
 * unaccessible after this action. Returns 0 on success, -1 otherwise.
 */
int cchannel_put(cchannel_t **ch, void *result);



/*
 * ctpool is a lightweight threadpool based on cqueue and pthreads. Currently
 * it is expected to persist the whole application lifetime.
 */

/*
 * Handle for ctpool.
 */
struct ctpool_t;
typedef struct ctpool_t ctpool_t;


/*
 * Create threadpool with the provided threads quantity. NULL is returned on
 * error.
 */
ctpool_t *ctpool_create(int size);


/*
 * ctpool_routine_t represents a job for a worker thread. channel can be NULL.
 */
typedef void (*ctpool_routine_t)(cchannel_t *channel, void *args);


/*
 * Dispath a task to the provided ctpool. Zero is returned on success, -1
 * otherwise. args are expected to be created with cmalloc.
 *
 * cchannel to the task will be returned, provide NULL if it is not needed.
 */
int ctpool_dispatch(
        ctpool_t *ctp,
        ctpool_routine_t routine,
        void *args, cchannel_t **channel
);


/*
 * Send a finish signal to queue and wait for all the threads in the provided
 * pool to finish, then invalidates to ctpool_t handler. Will hang in case of
 * endless jobs provided to pool. Return Zero on success, -1 othrewise.
 */
int ctpool_destroy(ctpool_t *ctp);

#ifdef __cplusplus
    }
#endif

#endif
