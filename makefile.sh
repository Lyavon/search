#!/bin/bash

SOURCE_DIR="$(pwd)"
BUILD_DIR="${SOURCE_DIR}/build"

CMD="${1}"
if [ -z "${CMD}" ]
then
    CMD="all"
fi

if [ "${CMD}" = "all" ]
then
    [ ! -d "${BUILD_DIR}" ] && mkdir "${BUILD_DIR}"
    cd "${BUILD_DIR}" 
    cmake \
        -D CMAKE_BUILD_TYPE=Release \
        "${SOURCE_DIR}"
    cmake --build . --config Release
fi

if [ "${CMD}" = "clean" ]
then
    rm -rf "${BUILD_DIR}"
fi