#include <threads.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <dirent.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdbool.h>
#include <semaphore.h>
#include <pthread.h>

#include "crutch.h"

#define BUF_SIZE 4096
#define ERROR_BUF_SIZE 1024
#define MAX_OPEN_FILES 100
#define MAX_SCAN_THREADS 24
#define MAX_DISCOVERY_THREADS 12
#define COLOR_YELLOW "\033[93m"
#define COLOR_ENDC "\033[0m"

struct opened_file {
    int fd;
    char *name;
    off_t size;
};

cqueue_t *opened_files_queue;
ctpool_t *scan_threadpool;
char *search_pattern;
size_t search_pattern_length;


/*
 * Destroy the provided node and replace pointer to it with the next node.
 */
int opened_file_destroy(void *of)
{
    int rc;
    struct opened_file *node = of;

    if (node == NULL)
        return 0;

    cdecref(&node->name);
    rc = close(node->fd);
    return rc;
}


/*
 * Create new struct opened_file with corresponding fields.
 * fname is copied. next sets to NULL.
 *
 * struct should be desrtoyed with opened_file_destroy.
 */
struct opened_file *opened_file_create(int fd, const char *fname, off_t size)
{
    struct opened_file *node;
    char *name;

    name = cstr_strcpy(fname);
    if (name == NULL) {
        print_e("Can't allocate buffer for name.\n");
        return NULL;
    }

    node = cmalloc(sizeof(struct opened_file), opened_file_destroy);
    if (node == NULL) {
        print_e("Can't allocate struct opened_file.\n");
        cdecref(&name);
        return NULL;
    }

    node->fd = fd;
    node->name = name;
    node->size = size;
    return node;
}


/*
 * Git-style check whether the file is binary.
 */
bool is_binary(const char *buf, ssize_t length)
{
    return !!memchr(buf, 0, length);
}


/*
 * Filter out all the undesired files and directories to scan.
 */
bool should_skip(const char *name)
{
    return !strcmp(name, ".") || !strcmp(name, "..");
}


/*
 * Recursively discover text files to scan.
 */
void file_discovery(const char *cwd, DIR *dir)
{
    DIR *subdir;
    struct dirent *entry;
    struct stat st;
    int fd;
    char *path;
    void *buf;
    struct opened_file *opened_file;

    while ((entry = readdir(dir)) != NULL) {
        if (should_skip(entry->d_name))
            continue;

        path = cstr_raw_join( cwd, "/", entry->d_name, NULL);

        if (path == NULL) {
            print_e("Can't join %s and %s. Skippping...\n", cwd, entry->d_name);
            return;
        }

        switch (entry->d_type) {
        case DT_DIR:
            subdir = opendir(path);
            if (subdir == NULL) {
                print_e("Can't open directory %s. Skipping...\n", path);
                break;
            }
            file_discovery(path, subdir);
            cdecref(&path);
            break;
        case DT_REG:
            fd = open(path, O_RDONLY);
            if (fd < 0) {
                print_e("Can't open file %s. Skipping...\n", path);
                break;
            }

            if (fstat(fd, &st)) {
                print_e("Can't fstat file %s. Skipping...\n", path);
                close(fd);
                break;
            }

            buf = mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
            if (buf == MAP_FAILED) {
                print_e("Can't mmap file %s. Skipping...\n", path);
                close(fd);
                break;
            }

            if (is_binary(buf, st.st_size)) {
                close(fd);
                munmap(buf, BUF_SIZE);
                cdecref(&path);
                break;
            }

            opened_file = opened_file_create(fd, path, st.st_size);
            if (opened_file == NULL) {
                print_e("Can't create new struct opened_file.\n");
                close(fd);
                munmap(buf, BUF_SIZE);
                cdecref(&path);
                break;

            }

            if (cqueue_put(opened_files_queue, opened_file)) {
                print_e("Can't put new item in queue.\n");
                close(fd);
                munmap(buf, BUF_SIZE);
                cdecref(&path);
                cdecref(&opened_file);
                break;
            }

            munmap(buf, BUF_SIZE);
            cdecref(&path);
        }
    }

    closedir(dir);
}


/*
 * Substring search in part of the string.
 * Return pointer to substring occurence if found thereof, NULL otherwise.
 */
char *strstrn2(
        const char *array,
        size_t array_length,
        const char *pattern,
        size_t pattern_length
        )
{
    size_t i;

    if (array_length < pattern_length)
        return NULL;

    for (i = 0; i < array_length - pattern_length + 1; i++) {
        if (array[i] != pattern[0])
            continue;
        if (!strncmp(&array[i], pattern, pattern_length))
            return (char *)&array[i];
    }
    return NULL;
}


/*
 * Present pattern occurence user friendly.
 */
void report(
        const char *name,
        size_t line_number,
        const char *line,
        off_t pattern_offset
        )
{
    printf("%s +%lu : %.*s%s%s%s%s\n",
            name, line_number,
            (int)pattern_offset, line,
            COLOR_YELLOW,
            search_pattern,
            COLOR_ENDC,
            line + pattern_offset + search_pattern_length
            );
}


/*
 * Scan for pattern a provided file.
 */
void scan(struct opened_file *file)
{
    char *file_contents, *pattern_occurence;
    char *line_begin, *line_end, *copied_line;
    size_t line_counter = 1, line_length;
    bool cr_found;

    file_contents = mmap(
            NULL,
            file->size,
            PROT_READ,
            MAP_PRIVATE,
            file->fd,
            0
            );
    if (file_contents == MAP_FAILED) {
        print_e("Can't mmap target %s. Skipping...\n", file->name);
        return;
    }

    line_begin = &file_contents[0];
    do {
        line_end = strpbrk(line_begin, "\r\n");
        if (line_end == NULL)
            break;
        cr_found = (*line_end == '\r');
        line_length = line_end - line_begin;

        pattern_occurence = strstrn2(
                line_begin,
                line_length,
                search_pattern,
                search_pattern_length
                );
        if (pattern_occurence != NULL) {
            copied_line = cstr_strncpy(line_begin, line_length);
            if (copied_line == NULL) {
                print_e("Can't copy line. Skipping...\n");
                munmap(file_contents, file->size);
                return;
            }

            report(
                    file->name,
                    line_counter,
                    copied_line,
                    pattern_occurence - line_begin
                    );
            cdecref(&copied_line);
        }

        line_begin = cr_found ? line_end + 2 : line_end + 1;
        line_counter++;
    } while (line_end < file_contents + file->size - 1);

    munmap(file_contents, file->size);
}


/*
 * file_scan_thread endlessly obtains files to scan from search_queue when
 * there are any.
 * The signal to stop is empty queue with permission to read from it. One
 * semaphore increment per thread.
 */
int file_scan_thread(void)
{
    struct opened_file *target;

    for (
            target = cqueue_get(opened_files_queue);
            target != NULL; /* Empty queue is a signal to stop */
            target = cqueue_get(opened_files_queue)) {
        scan(target);
        cdecref(&target);
    }

    return 0;
}

/*
 * file_discovery_thread recursively walks a given directory and dispatches
 * their contents.
 *
 * Since consumers run in unconditional loops, the stop sign is the empty queue
 * with permission to read from it. One semaphore increment per thread.
 */
int file_discovery_thread(void *args)
{
    DIR *cwd;
    char *path;
    int i;

    path = args;

    cwd = opendir(path);
    if (cwd == NULL) {
        print_e("Can't open dir %s.\n", path);
        exit(1);
    }
    file_discovery(path, cwd);

    for (i = 0; i < MAX_SCAN_THREADS; i++)
        cqueue_put(opened_files_queue, NULL);
    return 0;
}

/*
 * The main idea of the program is to speed up search in a vast project.
 * Sometimes searching with tools with regex support, such as grep, is too
 * time consuming. By restricting search to a substring it is possible to
 * drastically improve perfomance.
 *
 * search implements a producer-consumer model.
 *
 * Producer is a file_discovery_thread, who uses a specialized queue to
 * dispatch files to scan to file_scan_threads.
 *
 * Consumers are file_scan_threads of a defined quantity, that scan files from
 * queue for pattern occurences.
 *
 * TODO:
 * - add more producers?
 * - make 1-st mmap not go to waste.
 */
int main(int argc, char *argv[])
{
    int i;
    thrd_t d_thrd;
    char *root_directory;
    ctpool_task_t file_scan_task = {file_scan_thread, NULL, NULL};

    if (argc < 2) {
        printf("usage: %s [path] pattern\n", argv[0]);
        return 1;
    }

    if (argc < 3) {
        root_directory = strdup(".");
        search_pattern_length = strlen(argv[1]);
        search_pattern = (char *)argv[1];
    } else {
        root_directory = strdup(argv[1]);
        search_pattern_length = strlen(argv[2]);
        search_pattern = (char *)argv[2];
    }

    opened_files_queue = cqueue_create(MAX_OPEN_FILES);
    if (opened_files_queue == NULL) {
        print_e("Can't initialize opened_files_queue\n");
        exit(1);
    }

    if (thrd_create(&d_thrd, file_discovery_thread, root_directory) != thrd_success) {
        print_e("Can't create discovery thread\n");
        exit(1);
    }

    scan_threadpool = ctpool_create(MAX_SCAN_THREADS);
    if (scan_threadpool == NULL) {
        print_e("Can't initialize scan_threadpool\n");
        exit(1);
    }

    for (i = 0; i < MAX_SCAN_THREADS; i++) {
        if (ctpool_dispatch(scan_threadpool, &file_scan_task)) {
            print_e("Can't dispatch file_scan_task to the threadpool\n");
            exit(1);
        }
    }

    thrd_join(d_thrd, NULL);
    return ctpool_destroy(scan_threadpool);
}
