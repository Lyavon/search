#include <pthread.h>
#include <stdlib.h>
#include <stdbool.h>

#include "crutch.h"


typedef struct ccounter_t {
    ssize_t value;
    pthread_mutex_t access;
} ccounter_t;


int ccounter_destroy(void *counter)
{
    ccounter_t *cnt = counter ;
    return pthread_mutex_destroy(&cnt->access);
}


ccounter_t *ccounter_create(ssize_t initial_value)
{
    ccounter_t *cnt;

    cnt = cmalloc(sizeof(*cnt), ccounter_destroy);
    if (cnt == NULL) {
        print_e("Can't allocate new ccounter_t\n");
        return NULL;
    }

    if (pthread_mutex_init(&cnt->access, NULL)) {
        print_e("Can't initialize mutex in ccounter\n");
        cdecref(&cnt);
        return NULL;
    }

    cnt->value = initial_value;
    return cnt;
}


int ccounter_add(ccounter_t *cnt, ssize_t value)
{
    if (pthread_mutex_lock(&cnt->access)) {
        print_e("Can;t lock ccounter's mutex\n");
        return -1;
    }

    cnt->value += value;

    if (pthread_mutex_unlock(&cnt->access)) {
        print_e("Can;t lock %p ccounter's mutex\n", cnt);
        return -1;
    }
    return 0;
}


int ccounter_subtract(ccounter_t *cnt, ssize_t value)
{
    return ccounter_add(cnt, -value);
}


int ccounter_inc(ccounter_t *cnt)
{
    return ccounter_add(cnt, 1);
}


int ccounter_dec(ccounter_t *cnt)
{
    return ccounter_add(cnt, -1);
}


int ccounter_value(ccounter_t *cnt, ssize_t *res)
{
    if (pthread_mutex_lock(&cnt->access)) {
        print_e("Can't lock %p ccounter's mutex\n", cnt);
        return -1;
    }

    *res = cnt->value;

    if (pthread_mutex_unlock(&cnt->access)) {
        print_e("Can't lock %p ccounter's mutex\n", cnt);
        return -1;
    }

    return 0;
}


int ccounter_is_zero(ccounter_t *cnt, bool *res)
{
    ssize_t value;

    if (ccounter_value(cnt, &value)) {
        print_e("Can't obtain counter resurlt at %p\n", cnt);
        return -1;
    }

    *res = !! value;
    return 0;
}
