#ifndef CRUTCH_DEBUG_H
#define CRUTCH_DEBUG_H

#ifdef __cplusplus
    extern "C" {
#endif

#include <stdio.h>

#ifdef DEBUG
    #define print_d(format, ...) do { \
        fprintf(stdout, "%s +%d, %s(): ", __FILE__, __LINE__, __FUNCTION__); \
        fprintf(stdout, (format), ##__VA_ARGS__); \
        fflush(stdout); \
    } while(0)
#else
    #define print_d(format, ...)
#endif

#define print_e(format, ...) do { \
    fprintf(stderr, "%s +%d, %s() Error: ", __FILE__, __LINE__, __FUNCTION__); \
    fprintf(stderr, (format), ##__VA_ARGS__); \
} while(0)

#ifdef __cplusplus
    }
#endif

#endif