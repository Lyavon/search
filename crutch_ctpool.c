#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>

#include "crutch.h"


typedef struct ctpool_t {
    int size;
    cqueue_t *tasks;
    pthread_t threads[];
} ctpool_t;


/*
 * args are expected to be allocated by the cmalloc.
 */
typedef struct ctpool_task_t {
    ctpool_routine_t routine;
    cchannel_t *channel;
    void *args;
} ctpool_task_t;


/*
 * NULL task is considered a terminating signal.
 */
static void *ctpool_base_function(void *task_queue)
{
    cqueue_t *tasks = task_queue;
    ctpool_task_t *task;
    while (1) {
        task = cqueue_get(tasks);
        if (task == NULL || task->routine == NULL)
            break;

        task->routine(task->channel, task->args);

        if (cdecref(&task))
            print_e("Can't deref ctpool task\n");
    }

    return NULL;
}


ctpool_t *ctpool_create(int size)
{
    ctpool_t *ctp;
    int i, j;

    if (size < 1)
        return NULL;

    ctp = malloc(sizeof(*ctp) + sizeof(pthread_t) * size);
    if (ctp == NULL) {
        print_e("Can't allocate enough memory for ctpool_t.\n");
        return NULL;
    }

    ctp->size = size;

    ctp->tasks = cqueue_create(0);
    if (ctp->tasks == NULL) {
        print_e("Can't create task queue for ctpool_t.\n");
        goto cqueue_create_error;
    }

    for (i = 0; i < size; i++) {
        if (pthread_create(
            &ctp->threads[i],
            NULL,
            ctpool_base_function,
            ctp->tasks)
        ) {
            print_e("Can't create %d-th thread for ctpool_t.\n", i);
            goto pthread_create_error;
        }
    }

    return ctp;

pthread_create_error:
    for (j = 0; j < i; j++)
        if (pthread_kill(ctp->threads[j], SIGTERM))
            print_e("Can't destroy %d-th thread for ctpool_t properly.\n", j);
cqueue_create_error:
    free(ctp);
    return NULL;
}


static int ctpool_task_destructor(void *ctpool_task)
{
    ctpool_task_t *task = ctpool_task;
    return cdecref(&task->args);
}


int ctpool_dispatch(ctpool_t *ctp, ctpool_routine_t routine, void *args, cchannel_t **channel)
{
    ctpool_task_t *task = cmalloc(sizeof(*task), ctpool_task_destructor);

    if (task == NULL) {
        print_e("Can't allocate new ctpool_task_t\n");
        return -1;
    }

    task->args = args;
    task->routine = routine;

    if (channel == NULL) {
        task->channel = NULL;
        return cqueue_put(ctp->tasks, task);
    }

    *channel = cchannel_create();
    if (*channel == NULL) {
        print_e("Can't allocate cchannel_t\n");
        cdecref(&task);
        return -1;
    }

    task->channel = *channel;

    return cqueue_put(ctp->tasks, task);
}


int ctpool_destroy(ctpool_t *ctp)
{
    int i;

    for (i = 0; i < ctp->size; i++) {
        if (ctpool_dispatch(ctp, NULL, NULL, NULL)) {
            print_e("Can't dispatch NULL to the ctpool at %p.\n", ctp);
            return -1;
        }
    }

    for (i = 0; i < ctp->size; i++) {
        if (pthread_join(ctp->threads[i], NULL)) {
            print_e(
                "Can't properly finish %d-th thread at %p ctpool.\n",
                i,
                ctp);
            return -1;
        }
    }

    if (cqueue_destroy(ctp->tasks)) {
        print_e("Can't destroy task queue at %p ctpool.\n", ctp);
        return -1;
    }

    free(ctp);
    return 0;
}


int ctpool_join(ctpool_t *ctp)
{
    int i;

    for (i = 0; i < ctp->size; i++) {
        if (pthread_join(ctp->threads[i], NULL)) {
            print_e("Can't join thread %d at %p ctpool.\n", i, ctp);
            return -1;
        }
    }
    return 0;
}
