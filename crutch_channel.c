#include <semaphore.h>
#include <stdlib.h>
#include "crutch.h"
typedef struct cchannel_t {
    sem_t available;
    void *result;
} cchannel_t;


cchannel_t *cchannel_create(void)
{
    cchannel_t *ch = malloc(sizeof(*ch));
    if (ch == NULL) {
        print_e("Can't allocate new cchannel_t\n");
        return NULL;
    }

    if (sem_init(&ch->available, 0, 0)) {
        print_e("Can't initialize semaphore in cchannel.\n");
        free(ch);
        return NULL;
    }

    return ch;
}


int cchannel_result(cchannel_t **ch, void **result)
{
    if (ch == NULL || *ch == NULL)
        return 0;

    if (sem_wait(&((*ch)->available))) {
        print_e("Can't wait %p cchannel's semaphore.\n", *ch);
        return -1;
    }
    if (sem_destroy(&(*ch)->available))
        print_e("Can't destroy %p cchannel's mutex\n", *ch);

    if (result != NULL)
        *result = (*ch)->result;
    else
        cdecref(&(*ch)->result);

    free(*ch);
    *ch = NULL;
    return 0;
}


int cchannel_put(cchannel_t **ch, void *result)
{
    if (ch == NULL || *ch == NULL)
        return 0;

    (*ch)->result = result;

    if (sem_post(&(*ch)->available)) {
        print_e("Can't post %p cchannel's semaphore.\n", *ch);
        return -1;
    }

    *ch = NULL;
    return 0;
}
