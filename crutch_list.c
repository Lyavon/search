#include <stdlib.h>

#include "crutch.h"
#include <stdint.h>

typedef struct clist_t {
    cdecref_t data_deref;
    struct cle_t *head;
    struct cle_t *tail;
    size_t len;
} clist_t;


typedef struct cle_t {
    struct cle_t *prev;
    struct cle_t *next;
    void *data;
} cle_t;



clist_t *clist_init(cdecref_t data_deref)
{
    clist_t *list = cmalloc(sizeof(*list), NULL);
    if (list == NULL) {
        print_e("Can't allocate new clist_t object\n");
        return NULL;
    }

    list->data_deref = data_deref;
    list->head = NULL;
    list->tail = NULL;
    list->len = 0;

    return list;
}


static cle_t *clist_head(clist_t *list)
{
    if (list == NULL)
        return NULL;
    return list->head;
}


static cle_t *cle_next(cle_t *le)
{
    if (le == NULL)
        return NULL;
    return le->next;
}


static cle_t *cle_prev(cle_t *le)
{
    if (le == NULL)
        return NULL;
    return le->prev;
}


clist_t *clist_ref(clist_t *list)
{
    return cref(list);
}


int clist_deref(clist_t **list)
{
    cle_t *head;
    cdecref_t data_deref;
    int rc = 0;
    size_t ref_cnt;

    if (list == NULL || *list == NULL)
        return 0;

    head = (*list)->head;
    data_deref = (*list)->data_deref;
    ref_cnt = cref_cnt(*list);

    rc = cderef((void **)list);
    if (rc) {
        print_e("Can't deref clist at %p\n", *list);
        rc++;
    }

    if (ref_cnt == 1) {
        cle_t *le, *next_le;
        for (
                le = head, next_le= cle_next(le);
                le != NULL;
                le = next_le, next_le = cle_next(next_le)) {
            if (data_deref(&le->data)) {
                print_e("Can't deref clist element at %p\n", le);
                rc++;
            }
            free(le);
        }
    }

    return rc;
}


int clist_length(clist_t *list)
{
    return list->len;
}


int clist_append(clist_t *list, void *data)
{
    cle_t *le;

    le = malloc(sizeof(*le));
    if (le == NULL) {
        print_e("Can't allocate new clist element\n");
        return -1;
    }

    if (list->tail != NULL) {
        le->prev = list->tail;
        list->tail->next = le;
    } else {
        le->prev = NULL;
        list->head = le;
    }
    le->next = NULL;
    le->data = data;
    list->tail = le;
    list->len++;

    return 0;
}

int clist_prepend(clist_t *list, void *data)
{
    cle_t *le;

    le = malloc(sizeof(*le));
    if (le == NULL) {
        print_e("Can't allocate new clist element\n");
        return -1;
    }

    if (list->head != NULL) {
        le->next = list->head;
        list->head->prev = le;
    } else {
        le->next = NULL;
        list->tail = le;
    }
    le->prev = NULL;
    le->data = data;
    list->head = le;
    list->len++;

    return 0;
}

int clist_insert(clist_t *list, ssize_t at, void *data)
{
    cle_t *le, *prev_le, *next_le;
    size_t offset =at >= 0 ? at : at + list->len;

    if (offset > list->len) {
        print_e("Offset is greater than list length (%ju)\n", list->len);
        return -1;
    }

    if (offset == 0)
        return clist_prepend(list, data);
    if (offset == list->len)
        return clist_append(list, data);

    le = malloc(sizeof(*le));
    if (le == NULL) {
        print_e("Can't allocate new clist element\n");
        return -1;
    }
    le->data = data;

    if (offset < list->len - offset) {
        for (prev_le = list->head; offset > 1; offset--)
            prev_le = prev_le->next;
        next_le = prev_le->next;
    } else {
        for (next_le = list->tail; offset < list->len - 1; offset++)
            next_le = next_le->prev;
        prev_le = next_le->prev;
    }

    le->prev = prev_le;
    le->next = next_le;
    prev_le->next = le;
    next_le->prev = le;

    list->len++;
    return 0;
}


void **clist_iter(clist_t *list)
{
    if (list->len)
        return (void **)((uint8_t *)list->head + offsetof(cle_t, data));
    return NULL;
}


void **clist_iter_rev(clist_t *list)
{
    if (list->len)
        return (void **)((uint8_t *)list->tail + offsetof(cle_t, data));
    return NULL;
}


void **clist_next(void **le_data)
{
    if (le_data == NULL || *le_data == NULL)
        return NULL;
    cle_t *le = container_of(le_data, cle_t, data);
    if (le->next == NULL)
        return NULL;
    return (void **)((uint8_t *)le->next + offsetof(cle_t, data));
}


void **clist_prev(void **le_data)
{
    if (le_data == NULL || *le_data == NULL)
        return NULL;
    cle_t *le = container_of(le_data, cle_t, data);
    if (le->prev == NULL)
        return NULL;
    return (void **)((uint8_t *)le->prev + offsetof(cle_t, data));
}
