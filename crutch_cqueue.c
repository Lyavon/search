#include <semaphore.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "crutch.h"

/*
 * cqueue_item_t is an element of the unidirectional linked list for cqueue.
 * data is expected to be deleted by the end user.
 */
typedef struct cqueue_item_t {
    struct cqueue_item_t *next;
    void *data;
} cqueue_item_t;


/*
 * cqueue_t is a structure representing cqueue. head and tail are both NULL in
 * case of the empty queue.
 */
typedef struct cqueue_t {
    pthread_mutex_t access;
    sem_t items_available;
    sem_t capacity_left;
    bool limited_size;
    cqueue_item_t *head;
    cqueue_item_t *tail;
} cqueue_t;


static inline int cqueue_init(cqueue_t *cq, int max_size)
{
    if (max_size > 0) {
        cq->limited_size = true;
        if (sem_init(&cq->capacity_left, 0, max_size)) {
            print_e("Can't initialize upper boundary semaphore\n");
            return -1;
        }
    } else {
        cq->limited_size = false;
    }

    if (sem_init(&cq->items_available, 0, 0)) {
        print_e("Can't initialize semaphore in cqueue structure.\n");
        goto lower_boundary_sem_init_error;
    }

    if (pthread_mutex_init(&cq->access, NULL)) {
        print_e("Can't initialize mutext in cqueue structure.\n");
        goto mtx_init_error;
    }

    cq->head = NULL;
    cq->tail = NULL;
    return 0;

mtx_init_error:
    if (sem_destroy(&cq->items_available))
        print_e("Can't destroy cqueue semaphore properly.\n");
lower_boundary_sem_init_error:
    if (cq->limited_size && sem_destroy(&cq->capacity_left))
        print_e("Can't destroy upper boundary semaphore properly.\n");
    return -1;
}


cqueue_t *cqueue_create(int max_size)
{
    cqueue_t*cq = malloc(sizeof(*cq));
    if (cq == NULL) {
        print_e("Can't allocate new cqueue object.\n");
        return NULL;
    }

    if (cqueue_init(cq, max_size)) {
        print_e("Can't initialize cqueue structure.\n");
        free(cq);
        return NULL;
    }

    return cq;
}


int cqueue_destroy(cqueue_t *cq)
{
    bool mtx_error, upper_sem_error, lower_sem_error;

    if (mtx_error = pthread_mutex_destroy(&cq->access))
        print_e("Can't destroy mutext in cqueue at %p.\n", cq);

    if (upper_sem_error =
            cq->limited_size ? sem_destroy(&cq->capacity_left) : false)
        print_e(
            "Can't destroy upper boundary semaphore in cqueue at %p.\n",
            cq);

    if (cq->head != NULL)
        print_e("Queue is not empty in cqueue at %p.\n", cq);

    if (lower_sem_error = sem_destroy(&cq->items_available))
        print_e(
            "Can't destroy lower boundary semaphore in cqueue at %p.\n",
            cq);

    free(cq);

    return mtx_error || upper_sem_error || lower_sem_error ? -1 : 0;
}


int cqueue_put(cqueue_t *cq, void *data)
{
    cqueue_item_t *cle = malloc(sizeof(*cle));
    if (cle == NULL) {
        print_e("Can't allocate space for new queue item for %p cqueue.\n", cq);
        return -1;
    }

    cle->next = NULL;
    cle->data = data;

    if (cq->limited_size && sem_wait(&cq->capacity_left)) {
        print_e("Can't decrement %p cqueue upper semaphore\n", cq);
        return -1;
    }

    if (pthread_mutex_lock(&cq->access)) {
        print_e("Can't lock %p cqueue's mutex.\n", cq);
        goto mtx_error;
    }

    if (cq->head == NULL) {
        cq->head = cle;
        cq->tail = cle;
    } else {
        cq->tail->next = cle;
        cq->tail = cle;
    }

    if (sem_post(&cq->items_available)) {
        print_e("Can't increment %p cqueue's semaphore\n", cq);
        goto sem_post_error;
    }

    if (pthread_mutex_unlock(&cq->access)) {
        print_e("Can't unlock %p cqueue's mutex.\n", cq);
        goto mtx_error;
    }

    return 0;

sem_post_error:
    if (pthread_mutex_unlock(&cq->access))
        print_e("Can't unlock %p cqueue's mutex.\n", cq);
mtx_error:
    if (cq->limited_size && sem_post(&cq->capacity_left))
        print_e("Can't increment %p cqueue's uppper semaphore back.\n", cq);
    free(cle);
    return -1;
}


void *cqueue_get(cqueue_t *cq)
{
    void *data = NULL;
    cqueue_item_t *cle;

    if (sem_wait(&cq->items_available)) {
        print_e("Can't decrement %p cqueue's lower semaphore.\n", cq);
        return NULL;
    }

   if (pthread_mutex_lock(&cq->access)) {
        print_e("Can't lock %p cqueue's mutex.\n", cq);
        goto access_lock_error;
    }

    cle = cq->head;
    if (cle != NULL) {
        data = cle->data;
        cq->head = cq->head->next;
        free(cle);
    }

    if (cq->limited_size && sem_post(&cq->capacity_left)) {
        print_e("Can't increment %p cqueue's upper semaphore.\n", cq);
        goto capacity_increment_error;
    }

    if (pthread_mutex_unlock(&cq->access)) {
        print_e("Can't unlock %p cqueue's mutex.\n", cq);
        return NULL;
    }

    return data;

capacity_increment_error:
    if (pthread_mutex_unlock(&cq->access))
        print_e("Can't unlock %p cqueue's mutex back.\n", cq);
access_lock_error:
    if (sem_post(&cq->items_available))
        print_e("Can't increment %p cqueue's lower semaphore back.\n", cq);
    return NULL;
}


int cqueue_size(cqueue_t *cq)
{
    int size;

    if (pthread_mutex_lock(&cq->access)) {
        print_e("Can't lock %p cqueue's mutex.\n", cq);
        return -1;
    }

    if (sem_getvalue(&cq->items_available, &size)) {
        print_e("Can't obtain cqueue size\n");
        if (pthread_mutex_unlock(&cq->access))
            print_e("Can't unlock %p cqueue's mutex.\n", cq);
        return -1;
    }

    if (pthread_mutex_unlock(&cq->access)) {
        print_e("Can't unlock %p cqueue's mutex.\n", cq);
        return -1;
    }

    return size;
}
